package de.calmdevelopment.day3

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode
class BitString {
    StringBuilder bits = new StringBuilder()

    BitString() {}

    BitString(String input) {
        append(input)
    }

    void append(String string) {
        bits.append(string)
    }

    @Override
    String toString() {
        bits.toString()
    }

    Integer toInteger() {
        Integer.parseInt(bits.toString(),2)
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        BitString bitString = (BitString) o

        if (bits.toString() != bitString.bits.toString()) return false

        return true
    }

    int hashCode() {
        return (bits != null ? bits.toString().hashCode() : 0)
    }
}
