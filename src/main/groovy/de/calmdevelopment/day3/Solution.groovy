package de.calmdevelopment.day3

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day3-input")
        def dataList = reader.readDiagnosticData()
        println "current power consumption ${dataList.powerConsumption()}"
        println "current life support rate ${dataList.lifeSupportRating()}"
    }
}
