package de.calmdevelopment.day3

class DiagnosticDataList extends ArrayList<BitString> {
    boolean mostCommonAt(int index) {
        int ones = this.collect { it.bits[index] }.count { it == "1"}
        int zeros = this.size() - ones
        ones >= zeros
    }

    boolean leastCommonAt(int index) {
        !mostCommonAt(index)
    }

    int gammaRate() {
        def rate = toRateBitString({ it -> mostCommonAt(it) })
        rate.toInteger()
    }

    int epsilonRate() {
        def rate = toRateBitString {it -> leastCommonAt(it)}
        rate.toInteger()
    }

    private BitString toRateBitString(Closure method) {
        BitString rate = new BitString()
        (0..<first().bits.length()).each { rate.append(method.call(it) ? "1" : "0") }
        rate
    }

    int powerConsumption() {
        gammaRate() * epsilonRate()
    }

    DiagnosticDataList getMostCommonSublist(int index) {
        String bitCriteria = mostCommonAt(index) ? "1" : "0"
        this.findAll {it.toString()[index] == bitCriteria} as DiagnosticDataList
    }

    DiagnosticDataList getLeastCommonSublist(int index) {
        String bitCriteria = leastCommonAt(index) ? "1" : "0"
        this.findAll {it.toString()[index] == bitCriteria} as DiagnosticDataList
    }

    int oxygenGeneratorRating() {
        def list = this
        (0..<this.first().toString().length()).each {
            def mostCommonList = list.getMostCommonSublist(it)

            if (mostCommonList.size() > 0) {
                list = mostCommonList
            }
        }
        list.first().toInteger()
    }

    int co2ScrubberRating() {
        def list = this
        (0..<this.first().toString().length()).each {
            def leastCommonList = list.getLeastCommonSublist(it)

            if (leastCommonList.size() > 0) {
                list = leastCommonList
            }
        }
        list.first().toInteger()
    }

    int lifeSupportRating() {
        oxygenGeneratorRating() * co2ScrubberRating()
    }
}
