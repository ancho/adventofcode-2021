package de.calmdevelopment.day4

import de.calmdevelopment.day5.Grid
import groovy.transform.ToString

@ToString
class BingoBoard extends Grid<Cell> {

    int lastDraw = -1
    int bingoRow = -1
    boolean bingo = false
    int bingoCol = -1

    void addRow(String line) {
        addRow(line.split(" ").findAll { !it.isEmpty() }.collect { new Cell(value: it as int) })
    }


    void draw(int number) {
        rows().each { row -> row.each { col -> col.draw(number) } }
        lastDraw = number
    }

    boolean win() {
        checkBingoInRows()
        checkBingoInColumns()
        bingo
    }

    private List<Integer> checkBingoInColumns() {
        (0..rows().size()).each { colIndex ->
            checkBingo(column(colIndex), colIndex, false)
        }
    }


    private void checkBingoInRows() {
        rows().eachWithIndex { row, index ->
            checkBingo(row, index, true)
        }
    }

    private void checkBingo(List<Cell> values, int index, boolean row) {
        if (!bingo && hasBingo(values)) {
            bingo = true
            if (row) {
                bingoRow = index
            } else {
                bingoCol = index
            }
        }
    }

    boolean hasBingo(List<Cell> values) {
        values.findAll { it -> it?.marked }.size() == rows().size()
    }

    List<Integer> wonNumbers() {
        if (bingo) {
            if (bingoRow > -1) {
                return row(bingoRow).collect { it -> it.value }
            } else {
                return column(bingoCol).collect { it.value }
            }
        }
        []
    }

    int score() {
        def unmarkedSum = rows().collect {row->
            row.findAll {!it.marked}
        }.flatten().sum {it?.value}
        unmarkedSum * lastDraw
    }
}
