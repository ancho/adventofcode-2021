package de.calmdevelopment.day4

import de.calmdevelopment.day5.Point
import de.calmdevelopment.day5.Value
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@ToString
@EqualsAndHashCode
class Cell implements Value {
    Point location = new Point(0,0)
    int value = 0
    boolean marked = false

    void draw(int number) {
        if (value == number) {
            marked = true
        }
    }

    @Override
    void increase() {
        value++
    }
}
