package de.calmdevelopment.day4

class BingoGame {
    BingoContext context

    BingoGame(BingoContext bingoContext) {
        context = bingoContext
    }

    BingoBoard solve() {
        for (int draw : context.draws) {
            for (BingoBoard board : context.boards) {
                board.draw(draw)
                if (board.win()) return board
            }
        }
        new BingoBoard()
    }

    BingoBoard solveLast() {
        for (int draw : context.draws) {
            for (BingoBoard board : context.boards) {
                board.draw(draw)
                if (board.win()) {
                    if (context.boards.count {it.bingo} == context.boards.size()) return board
                }
            }
        }
        new BingoBoard()
    }
}
