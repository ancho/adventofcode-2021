package de.calmdevelopment.day4

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def context = new InputsFileReader("day4-input").readBingoDefinition()
        def game = new BingoGame(context)

        def board = game.solve()

        println "score: ${board.score()} lastDraw: ${board.lastDraw} won numbers: ${board.wonNumbers()}"

        board = game.solveLast()

        println "score: ${board.score()} lastDraw: ${board.lastDraw} won numbers: ${board.wonNumbers()}"
    }
}
