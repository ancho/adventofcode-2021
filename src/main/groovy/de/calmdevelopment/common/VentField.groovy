package de.calmdevelopment.common

import de.calmdevelopment.day5.Point

class VentField {
    Point start
    Point end

    VentField(String start, String end) {
        this.start = Point.from(start)
        this.end = Point.from(end)
    }
}
