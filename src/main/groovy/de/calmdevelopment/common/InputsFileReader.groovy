package de.calmdevelopment.common

import de.calmdevelopment.day1.Reader
import de.calmdevelopment.day2.Command
import de.calmdevelopment.day2.CommandValuePair
import de.calmdevelopment.day3.BitString
import de.calmdevelopment.day3.DiagnosticDataList
import de.calmdevelopment.day4.BingoBoard
import de.calmdevelopment.day4.BingoContext
import de.calmdevelopment.day4.Cell
import de.calmdevelopment.day9.HeightMap

class InputsFileReader implements Reader {
    String filename

    InputsFileReader(String filename) {
        this.filename = filename
    }

    @Override
    List<Integer> read() {
        readLines().collect { it as int }
    }

    @Override
    List<CommandValuePair> readCommands() {
        readLines().collect {
            def (command, value) = it.split(" ")
            Command cmd = Command.by(command)
            new CommandValuePair(command: cmd, value: value as int)
        }
    }

    @Override
    DiagnosticDataList readDiagnosticData() {
        readLines().collect {
            new BitString(it)
        } as DiagnosticDataList
    }

    @Override
    BingoContext readBingoDefinition() {
        def context = new BingoContext()
        def lines = readLines()
        def board = new BingoBoard()

        lines.eachWithIndex { String line, int index ->
            if ( index == 0 ) {
                context.draws = csvToIntList(line)
            } else if ( line.isEmpty() ) {
                board = new BingoBoard()
                context.addBoard(board)
            } else {
                board.addRow(line)
            }
        }
        context
    }

    @Override
    List<VentField> readVentCoordinates() {
        def fields = []
        readLines().each {
            def (start, end) = it.split("->")
            fields << new VentField(start.trim(),end.trim())
        }
        fields
    }

    @Override
    HeightMap readHeightMap() {
        HeightMap heightMap = new HeightMap()

        readLines().each { line ->
            heightMap.addRow(line.collect {new Cell(value: it as int) })
        }
        heightMap
    }

    static List<Integer> csvToIntList(String csv) {
        csv.split(",").collect { it as int }
    }

    List<String> readLines() {
        this.class.getResourceAsStream("/$this.filename")
                .readLines()
    }

    List<Integer> readListOfIntegers() {
        csvToIntList(readLines()[0])
    }
}
