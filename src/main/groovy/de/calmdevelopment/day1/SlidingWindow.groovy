package de.calmdevelopment.day1

class SlidingWindow {
    int size = 3
    int step = 1
    boolean keepRemainder = false

    List<Integer> create(List<Integer> inputs) {
        collateWindows(inputs).collect(window -> window.sum())
    }

    List<List<Integer>> collateWindows(List<Integer> inputs) {
        inputs.collate(size, step, keepRemainder)
    }
}
