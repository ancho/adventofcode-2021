package de.calmdevelopment.day1

class SonarSweep {
    Reader reader

    SonarSweep(Reader reportFileReader) {
        this.reader = reportFileReader
    }

    List<Integer> report() {
        reader.read()
    }

    List<Integer> slidingWindowReport() {
        new SlidingWindow().create(report())
    }

    static int increases(List<Integer> depths) {
        new SlidingWindow(size: 2).collateWindows(depths).count { depthIncreased(it) }
    }

    private static boolean depthIncreased(List<Integer> measureTuple) {
        def (previous, current) = measureTuple
        previous < current
    }
}
