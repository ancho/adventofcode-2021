package de.calmdevelopment.day1

import de.calmdevelopment.common.VentField
import de.calmdevelopment.day2.CommandValuePair
import de.calmdevelopment.day3.DiagnosticDataList
import de.calmdevelopment.day4.BingoContext
import de.calmdevelopment.day9.HeightMap

interface Reader {
    List<Integer> read()

    List<CommandValuePair> readCommands()

    DiagnosticDataList readDiagnosticData()

    BingoContext readBingoDefinition()

    List<VentField> readVentCoordinates()

    HeightMap readHeightMap()
}
