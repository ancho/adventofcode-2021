package de.calmdevelopment.day1

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def sweep = new SonarSweep(new InputsFileReader("day1-input"))
        def report = sweep.report()
        int increases = sweep.increases(report)

        println "number of increases: $increases"

        report = sweep.slidingWindowReport()
        increases = sweep.increases(report)
        println "number of increases with sliding window: $increases"
    }
}
