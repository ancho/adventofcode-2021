package de.calmdevelopment.day5

import de.calmdevelopment.day4.Cell

class HydrothermalVentMap extends Grid<Cell> {
    void addVent(Point start, Point end) {
        growRows(start, end)
        growColumns(start, end)
        countVents(start, end)
    }

    private List<Point> countVents(Point start, Point end) {
        start.coverPoints(end).each { incrementAt(it) }
    }

    private void growRows(Point start, Point end) {
        int maxRows = Point.maxRows(start, end)
        if (rows().size() < maxRows) {
            (rows().size()..maxRows).each { row ->
                rows().add(new ArrayList<Cell>())
            }
        }
    }

    void growColumns(Point start, Point end) {
        def maxCols = Point.maxCols(start, end)
        rows().each { row ->
            if (row.size() <= maxCols) {
                (row.size()..maxCols).each { col ->
                    row.add(new Cell(value: 0))
                }
            }
        }
    }


    int numberMin2Overlaps() {
        rows().collect().flatten().count { it.value >= 2}
    }
}
