package de.calmdevelopment.day5

import groovy.transform.EqualsAndHashCode
import groovy.transform.Immutable
import groovy.transform.ToString

@ToString
@Immutable
@EqualsAndHashCode
class Point {
    int column
    int row

    static Point from(String coordinate) {
        def (x, y) = coordinate.split(",")
        new Point(x as int, y as int)
    }

    static int maxRows(Point start, Point end) {
        Math.max(start.row, end.row)
    }

    static int maxCols(Point start, Point end) {
        Math.max(start.column, end.column)
    }

    List<Point> coverPoints(Point other) {
        if (verticalTo(other)) {
            if (row <= other.row) {
                return coverVertical(row, other.row)
            } else {
                return coverVertical(other.row, row)
            }
        } else if (horizontalTo(other)) {
            if (column <= other.column) {
                return coverHorizontal(column, other.column)
            } else {
                return coverHorizontal(other.column, column)
            }
        } else if (diagonalTo(other)) {
            if (row <= other.row) {
                return coverDiagonal(column, other.column, row)
            } else {
                return coverDiagonal(other.column, column, other.row)
            }
        }
        []
    }

    private boolean horizontalTo(Point other) {
        row == other.row && column != other.column
    }

    private boolean verticalTo(Point other) {
        column == other.column && row != other.row
    }

    boolean diagonalTo(Point other) {
        (column == row && other.column == other.row) || (column != other.column && row != other.row)
    }

    List<Point> coverHorizontal(int from, int to) {
        (from..to).collect { new Point(it, row) }
    }

    List<Point> coverVertical(int from, int to) {
        (from..to).collect { new Point(column, it) }
    }

    List<Point> coverDiagonal(int from, int to, int startY) {
        def list = []
        (from..to).eachWithIndex { int x, int offset ->
            list << new Point(x, startY + offset)
        }
        return list

    }
}
