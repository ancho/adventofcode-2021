package de.calmdevelopment.day5

import de.calmdevelopment.day4.Cell

class Grid<T extends Cell> {
    private List<List<T>> grid = []

    List<T> column(col) {
        rows().collect { row -> row[col] }
    }

    List<T> row(int row) {
        grid[row]
    }

    void addRow(List<T> line) {
        line.eachWithIndex { it, col -> it.location = new Point(col, rowSize())}
        grid.add(line)
    }

    List<List<T>> rows() { grid }

    List<T> adjacentListAt(Point point) {
        def up = (point.row - 1 >= 0) ? getAt(point.row-1, point.column) : null
        def down = (point.row + 1 < rowSize()) ? getAt(point.row+1, point.column) : null
        def left = (point.column - 1 >= 0) ? getAt(point.row, point.column-1) : null
        def right = (point.column + 1 < columnSize()) ? getAt(point.row, point.column+1) : null

        [up, down, left, right].findAll { it != null }
    }

    List<T> allAdjacentListAt(Point point) {
        def up = (point.row - 1 >= 0) ? getAt(point.row-1, point.column) : null
        def upRight = (point.row-1 >= 0) && (point.column + 1 < columnSize()) ? getAt(point.row-1, point.column + 1) : null
        def right = (point.column + 1 < columnSize()) ? getAt(point.row, point.column+1) : null
        def downRight = (point.row + 1 < rowSize()) && (point.column + 1 < columnSize()) ? getAt(point.row+1, point.column+1) : null
        def down = (point.row + 1 < rowSize()) ? getAt(point.row+1, point.column) : null
        def downLeft = (point.row + 1 < rowSize()) && (point.column - 1 >= 0) ? getAt(point.row+1, point.column-1) : null
        def left = (point.column - 1 >= 0) ? getAt(point.row, point.column-1) : null
        def upLeft = (point.row-1 >= 0) && (point.column - 1 >= 0) ? getAt(point.row-1, point.column - 1) : null

        [up, upRight, right, downRight,  down, downLeft, left, upLeft].findAll { it != null }
    }



    void incrementAt(Point point) {
        getAt(point.row, point.column).increase()
    }

    T getAt(int row, int column) {
        rows()[row]?[column]
    }

    int columnSize() {
        (grid.size()>0) ? grid[0].size() : 0
    }

    int rowSize() {
        grid.size()
    }

    void unmarkAll() {
        rows().each { row ->
            row.each {it.marked = false}
        }
    }

    void reset() {
        grid.clear()
    }
}
