package de.calmdevelopment.day5

interface Value {
    void increase()
}