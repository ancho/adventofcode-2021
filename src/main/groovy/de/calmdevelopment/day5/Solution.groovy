package de.calmdevelopment.day5

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def fields = new InputsFileReader("day5-input").readVentCoordinates()

        HydrothermalVentMap map = new HydrothermalVentMap()

        fields.each {map.addVent(it.start, it.end)}

        println "numper of min 2 overlaps ${map.numberMin2Overlaps()}"
    }
}
