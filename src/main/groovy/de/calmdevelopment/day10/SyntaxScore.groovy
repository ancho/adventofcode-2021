package de.calmdevelopment.day10

class SyntaxScore {
    private List<String> lines

    def score = [
            ')': 3,
            ']': 57,
            '}': 1197,
            '>': 25137
    ]

    def incompleteScore = [
            ')': 1,
            ']': 2,
            '}': 3,
            ">": 4
    ]

    SyntaxScore(List<String> lines) {
        this.lines = lines
    }

    int corrupted() {

        lines.collect { new SyntaxCheck().check(it) }
                .findAll { it.corrupted }
                .collect { score[it.invalidChunk] }
                .sum()
    }

    long incomplete() {
        def scores = lines.collect { new SyntaxCheck().check(it) }
                .findAll { !it.complete && !it.corrupted }
                .collect {
                    Long result = 0
                    it.remainingClose.each { result = 5 * result + incompleteScore[it] }
                    result
                }

        scores.sort()[scores.size() / 2]
    }
}
