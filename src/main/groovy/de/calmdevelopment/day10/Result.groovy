package de.calmdevelopment.day10

class Result {
    boolean corrupted
    boolean complete
    String invalidChunk
    List<String> remainingClose = []
}

class Incomplete extends Result {
    Incomplete(List<String> remaining) {
        complete = false
        corrupted = false
        this.remainingClose = remaining
    }
}

class Complete extends Result {
    Complete() {
        complete = true
        corrupted = false
    }
}

class Corrupt extends Result {
    Corrupt(String invalidChunk) {
        complete = false
        corrupted = true
        this.invalidChunk = invalidChunk
    }
}
