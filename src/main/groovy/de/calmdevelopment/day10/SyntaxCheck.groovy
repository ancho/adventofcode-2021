package de.calmdevelopment.day10

class SyntaxCheck {

    def pairs = [
            '(': ')',
            '[': ']',
            '{': '}',
            '<': '>'
    ]

    Result check(String line) {

        def opened = []
        def expected = []


        for (String chunk : line.split("")) {

            if (chunk in pairs.keySet()) {
                opened.push(chunk)
                expected.push(pairs[chunk])
            }

            if (chunk in pairs.values()) {
                def expectedClosing = expected.pop()
                opened.pop()

                if (chunk != expectedClosing) {
                    return new Corrupt(chunk)
                }
            }
        }
        if ( opened.isEmpty() && expected.isEmpty() ) {
            return new Complete()
        }
        else {
            return new Incomplete(expected)
        }
    }
}
