package de.calmdevelopment.day10

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day10-input")
        def score = new SyntaxScore(reader.readLines())

        println "score: ${score.corrupted()}"
        println "score incomplete: ${score.incomplete()}"
    }
}
