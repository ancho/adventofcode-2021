package de.calmdevelopment.day2

class ForwardNavigation implements Navigation {
    @Override
    void navigate(Submarine submarine, int value) {
        submarine.horizontalPosition += value
        submarine.depth += (submarine.aim * value)
    }
}
