package de.calmdevelopment.day2

enum Command {

    FORWARD(new ForwardNavigation()),
    DOWN(new DownNavigation()),
    UP(new UpNavigation()),
    UNKNOWN(new UnknownNavigation())

    public Navigation navigation

    Command(Navigation navigation) {
        this.navigation = navigation
    }

    static Command by(String direction) {
        switch(direction) {
            case "forward": return FORWARD
            case "down": return DOWN
            case "up": return UP
            default: return UNKNOWN
        }
    }
}