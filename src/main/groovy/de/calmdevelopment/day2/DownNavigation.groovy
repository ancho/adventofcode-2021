package de.calmdevelopment.day2

class DownNavigation implements Navigation {
    @Override
    void navigate(Submarine submarine, int value) {
        submarine.aim += value
    }
}
