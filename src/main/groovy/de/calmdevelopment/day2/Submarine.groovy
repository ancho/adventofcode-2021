package de.calmdevelopment.day2

import groovy.transform.ToString

@ToString
class Submarine {
    int depth = 0
    int horizontalPosition = 0
    int aim = 0

    void navigate(CommandValuePair commandValuePair) {
        def navigation = commandValuePair.navigation()
        navigation.navigate(this, commandValuePair.value)
    }

    void navigate(List<CommandValuePair> commands) {
        commands.each {navigate(it)}
    }
}
