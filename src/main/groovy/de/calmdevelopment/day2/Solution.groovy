package de.calmdevelopment.day2

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day2-input")

        def submarine = new Submarine()
        submarine.navigate(reader.readCommands())
        def multiplay = submarine.depth * submarine.horizontalPosition
        println "$submarine (multiply: $multiplay)"
    }
}
