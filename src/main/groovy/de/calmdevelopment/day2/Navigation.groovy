package de.calmdevelopment.day2

interface Navigation {

    void navigate(Submarine submarine, int value)
}