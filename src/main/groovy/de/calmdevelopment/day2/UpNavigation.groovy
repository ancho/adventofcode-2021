package de.calmdevelopment.day2

class UpNavigation implements Navigation {
    @Override
    void navigate(Submarine submarine, int value) {
        if (submarine.aim >= value) {
            submarine.aim -= value
        }
        else {
            submarine.aim = 0
        }
    }
}
