package de.calmdevelopment.day2

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.Immutable
import groovy.transform.ToString

@Immutable
@EqualsAndHashCode
@ToString
@CompileStatic
class CommandValuePair {
    Command command
    int value

    Navigation navigation() {command.navigation}
}
