package de.calmdevelopment.day11

import de.calmdevelopment.day4.Cell
import de.calmdevelopment.day5.Grid

class OctopusMap extends Grid<Cell> {
    def step = 0
    private List<String> original

    OctopusMap(List<String> mapInput) {
        original = mapInput
        resetFromOriginal()
    }

    private List<String> resetFromOriginal() {
        reset()
        original.each { row ->
            addRow(row.collect { new Cell(value: it as int) })
        }
    }

    void step() {
        unmarkAll()
        increaseEnergyLevel()
        flashOctopuses()
        resetFlashedEnergyLevel()
        step++
    }

    void increaseEnergyLevel() {
        rows().each { row ->
            row.each { cell -> cell.increase() }
        }
    }

    void flashOctopuses() {
        def flashedCells = [] as List<Cell>
        rows().each { row ->
            flashedCells.addAll( row.findAll { it.value > 9 && !it.marked} )
        }

        flashedCells.each {it.marked = true}

        flashedCells.each {
            allAdjacentListAt(it.location).findAll{!it.marked}.each {it.increase()}
            flashOctopuses()
        }
    }

    void resetFlashedEnergyLevel() {
        def flashedCells = [] as List<Cell>

        rows().each { row ->
            flashedCells.addAll( row.findAll {it.marked})
        }

        flashedCells.each {it.value = 0}
    }

    int flashed() {
        rows().collect {row -> row.findAll { it.marked}.size() }.sum()
    }

    int countFlashes(int steps) {
        resetFromOriginal()
        (1..steps).collect {
            step()
            flashed()
        }.sum()
    }

    int firstSynchronizedStep() {
        resetFromOriginal()
        do {
            step()
        } while (flashed() != 100)
        step
    }
}
