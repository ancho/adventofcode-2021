package de.calmdevelopment.day11

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day11-input")
        def octopusMap = new OctopusMap(reader.readLines())

        println "flashes after 100 steps: ${octopusMap.countFlashes(100)}"
        println "first synchronous flashes at step: ${octopusMap.firstSynchronizedStep()}"
    }
}
