package de.calmdevelopment.day9

import de.calmdevelopment.day4.Cell
import de.calmdevelopment.day5.Grid

class HeightMap extends Grid<Cell> {
    void markLowest() {
        rows().eachWithIndex { List<Cell> row, int rowIndex ->
            row.eachWithIndex { Cell cell, int colIndex ->
                boolean allHigher = allAdjacentHigher(cell)
                if (allHigher) {
                    cell.marked = true
                }
            }
        }
    }

    private boolean allAdjacentHigher(cell) {
        def adjacentList = adjacentListAt(cell.location)
        boolean allHigher = adjacentList.collect { cell.value < it.value }.every()
        allHigher
    }

    int lowPointRiskLevel() {
        markLowest()

        rows().collect { row ->
            row.findAll {it.marked}.collect { it.value + 1 }.sum() ?: 0
        }.sum()
    }

    List<Cell> getLowest() {
        markLowest()
        def lowest = []
        rows().each {
            it.findAll {it.marked}.each { lowest << it}
        }
        lowest
    }

    Set<Cell> basin(Cell cell) {
        def result = [] as Set

        adjacentListAt(cell.location).each {
            if (it.value !=9 && !it.marked) {
                it.marked = true
                result.addAll(basin(it))
            }
        }
        if (cell.marked) result << cell
        result
    }

    int largestBasins() {
        def basinSizes = getLowest().collect( cell ->
                basin(cell).size()
        ).sort().reverse()
        basinSizes[0] * basinSizes[1] * basinSizes[2]
    }
}
