package de.calmdevelopment.day9

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day9-input")
        def map = reader.readHeightMap()

        println "low point risk level: ${map.lowPointRiskLevel()}"
        println "three largest basins: ${map.largestBasins()}"
    }
}
