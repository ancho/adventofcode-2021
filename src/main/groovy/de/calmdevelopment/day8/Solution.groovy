package de.calmdevelopment.day8

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day8-input")
        def lines = reader.readLines()
        def display = new SegmentDisplay()

        lines.each {
            display.addInputline(it)
        }

        def count = display.countMatchingInOutputValue([1,4,7,8])
        println "matching pattern count in display output values $count"

        def outputSum = display.inputs.collect {
            it.getOutputValues()
        }.sum()

        println "sum $outputSum"
    }
}
