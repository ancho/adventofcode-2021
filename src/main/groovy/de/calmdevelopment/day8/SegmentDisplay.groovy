package de.calmdevelopment.day8

class SegmentDisplay {
    def segments = [
            0: "abcefg",
            1: "cf",
            2: "acdeg",
            3: "acdfg",
            4: "bcdf",
            5: "abdfg",
            6: "abdefg",
            7: "acf",
            8: "abcdefg",
            9: "abcdfg"
    ]

    List<Entry> inputs = []

    void addInputline(String entry) {
        def splitted = entry.split("\\|")
        def signalPatterns = splitted[0].trim().split(" ") as List
        def outputValue = splitted[1].trim().split(" ") as List

        inputs << new Entry(signalPatterns, outputValue)
    }

    int countMatchingInOutputValue(List<Integer> digit) {
        digit.sum { number ->
            int search = segments[number].length()
            inputs.sum {
                it.outputValue.count {
                    it.length() == search
                }
            }
        }
    }
}
