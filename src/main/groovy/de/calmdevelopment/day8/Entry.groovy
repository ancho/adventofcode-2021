package de.calmdevelopment.day8

class Entry {
    List<String> signalPatterns
    List<String> outputValue
    def patterns = [:] as Map<Integer, String>

    Entry(List<String> signalPatterns, List<String> outputValue) {
        this.outputValue = outputValue
        this.signalPatterns = signalPatterns
        createSegmentsMap()
    }

    void createSegmentsMap() {
        patterns.putAll(determineEasySegments())
        patterns.putAll(guessSixSegments())
        patterns.putAll(guessFiveSegments())
    }

    private Map<Integer, String> determineEasySegments() {
        def segments = [:] as Map<Integer, String>
        signalPatterns.toSorted() { it.size() }.each {
            switch (it.size()) {
                case 2:
                    segments[1] = sorted(it)
                    break
                case 3:
                    segments[7] = sorted(it)
                    break
                case 4:
                    segments[4] = sorted(it)
                    break
                case 7:
                    segments[8] = sorted(it)
                    break
            }
        }
        segments
    }

    Map<? extends Integer, ? extends String> guessFiveSegments() {
        def segments = [:] as Map<Integer, String>

        signalPatterns.findAll { it.size() == 5 }.each {
            def pattern = containsPattern(it, 1)
            if (pattern) {
                segments[3] = sorted(it)
            } else if (patterns[6] && patterns[9] && patterns[1]) {
                def bitUpperSegment = patterns[6].contains(patterns[1][0]) ? patterns[1][1] : patterns[1][0]

                if (patterns[9].replace(bitUpperSegment, "") == sorted(it)) {
                    patterns[5] = sorted(it)
                } else {
                    patterns[2] = sorted(it)
                }
            }
        }
        segments
    }

    Map<? extends Integer, ? extends String> guessSixSegments() {
        def segments = [:] as Map<Integer, String>

        signalPatterns.findAll { it.size() == 6 }.each {
            if (containsPattern(it, 4)) {
                segments[9] = sorted(it)
            } else if (containsPattern(it, 1) && !containsPattern(it, 4)) {
                segments[0] = sorted(it)
            } else {
                segments[6] = sorted(it)
            }
        }
        segments
    }


    static String sorted(String segments) {
        segments.toList().sort().join("")
    }

    boolean containsPattern(String inputPattern, int number) {
        patterns[number].collect {
            inputPattern.contains(it)
        }.every()
    }


    long getOutputValues() {
        outputValue.collect {
            patterns.find {pattern -> sorted(pattern.value) == sorted(it)}.key
        }.join("") as Long
    }
}
