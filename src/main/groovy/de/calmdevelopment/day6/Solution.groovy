package de.calmdevelopment.day6

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day6-input")
        def swarm = new LanternFishSwarm(reader.readListOfIntegers())

        swarm.populate(80)

        println "swarm after ${swarm.day} days: ${swarm.total()}"

        swarm = new LanternFishSwarm(reader.readListOfIntegers())

        swarm.populate(256)

        println "swarm after ${swarm.day} days: ${swarm.total()}"

    }
}
