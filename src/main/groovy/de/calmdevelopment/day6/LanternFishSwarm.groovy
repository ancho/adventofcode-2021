package de.calmdevelopment.day6

class LanternFishSwarm {

    def timers = (0..8).collect {0 as BigInteger}
    int day = 0

    LanternFishSwarm(List<Integer> swarmTimers) {
        swarmTimers.each {
            timers[it] += 1
        }
    }

    void nextDay() {
        day++
        def newFishes = timers.pop()
        timers[6] += newFishes
        timers.add(newFishes)
    }

    void populate(int days) {
        (1..days).each { nextDay() }
    }

    BigInteger total() {
        timers.sum()
    }
}
