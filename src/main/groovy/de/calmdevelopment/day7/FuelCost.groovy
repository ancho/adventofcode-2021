package de.calmdevelopment.day7

import groovy.transform.Memoized

class FuelCost {
    def positions = [:]

    FuelCost(List<Integer> horizontalPositions) {
        horizontalPositions.each {
            if (!positions.containsKey(it)) {
                positions[it] = 1L
            } else {
                positions[it] += 1L
            }
        }
    }

    int calculate(int index) {
        positions.collect {
            calcFuelForIndex(Math.abs(it.key - index), it.value)
        }.sum()
    }

    private static long calcFuelForIndex(long number, long value) {
        sumValuesToZero(number) * value
    }

    @Memoized
    private static long sumValuesToZero(long number) {
        (number..0).sum()
    }

    int leastFuelCost() {
        fuelCosts().sort().first()
    }

    List<Integer> fuelCosts() {
        def maxIndex = positions.keySet().max()
        (0..maxIndex).collect { calculate(it) }
    }
}
