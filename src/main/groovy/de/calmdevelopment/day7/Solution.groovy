package de.calmdevelopment.day7

import de.calmdevelopment.common.InputsFileReader

class Solution {
    static void main(String[] args) {
        def reader = new InputsFileReader("day7-input")
        def positions = reader.readListOfIntegers()
        def start = System.currentTimeMillis()
        def cost = new FuelCost(positions)
        def endInit = System.currentTimeMillis()
        println "initialized ${cost.positions.size()} positions in ${endInit-start} millis"
        def startCalc = System.currentTimeMillis()
        println "least fuel cost: ${cost.leastFuelCost()}"
        def end = System.currentTimeMillis()

        println "calculation done in ${end-startCalc} millis"
        println "done in ${end-start} millis"

    }
}
