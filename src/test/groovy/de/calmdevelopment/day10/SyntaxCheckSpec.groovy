package de.calmdevelopment.day10

import spock.lang.Specification

class SyntaxCheckSpec extends Specification {
    def "should return result with status of check"() {
        given:
        def checker = new SyntaxCheck()

        when:
        Result result = checker.check("()")

        then:
        !result.corrupted
        result.complete
    }

    def "should be complete for more complex syntax lines"() {
        given:
        def checker = new SyntaxCheck()

        when:
        Result result = checker.check(validChunk)

        then:
        result.complete

        where:
        validChunk << ["{()()()}", "<([{}])>", "[<>({}){}[([])<>]]", "(((((((((())))))))))"]
    }

    def "should return currupted result"() {
        given:
        def checker = new SyntaxCheck()

        when:
        Result result = checker.check(invalidLine)

        then:
        result.invalidChunk == invalidChunk
        result.corrupted
        !result.complete

        where:
        invalidLine || invalidChunk
        "(]"        || "]"
        "{()()()>"  || ">"
        "(((()))}"  || "}"
        "<([]){()}[{}])" || ")"
    }

    def "should return incomplete result"() {
        given:
        def checker = new SyntaxCheck()

        when:
        Result result = checker.check(line)

        then:
        result.remainingClose == expected
        !result.corrupted
        !result.complete

        where:
        line || expected
        "[()" || [']']
        "[({(<(())[]>[[{[]{<()<>>" || ["}","}","]","]",")","}",")","]"]
        "<{([{{}}[<[[[<>{}]]]>[]]" || ["]",")","}",">"]
    }

    def "should fail on first corrupt chunk"() {
        given:
        def checker = new SyntaxCheck()

        when:
        Result result = checker.check("{([(<{}[<>[]}>{[]{[(<()>")

        then:
        result.invalidChunk == "}"
        result.corrupted
        !result.complete
    }

}
