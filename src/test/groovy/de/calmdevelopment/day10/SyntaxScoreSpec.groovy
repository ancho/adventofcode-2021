package de.calmdevelopment.day10

import spock.lang.Specification

class SyntaxScoreSpec extends Specification {
    def "should score corrupted lines"() {
        given:
        def lines = """
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
"""

        def syntaxScore = new SyntaxScore(lines.readLines())

        expect:
        syntaxScore.corrupted() == 26397
    }

    def "should score incomplete line"() {
        given:
        def syntaxScore = new SyntaxScore([
                "[({(<(())[]>[[{[]{<()<>>",
                "[(()[<>])]({[<{<<[]>>(",
                "(((({<>}<{<{<>}{[]{[]{}",
                "{<[[]]>}<{[{[{[]{()[[[]",
                "<{([{{}}[<[[[<>{}]]]>[]]"
        ])

        expect:
        syntaxScore.incomplete() == 288957
    }
}
