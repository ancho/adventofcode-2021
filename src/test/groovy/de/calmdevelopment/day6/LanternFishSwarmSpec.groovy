package de.calmdevelopment.day6

import spock.lang.Specification

class LanternFishSwarmSpec extends Specification {
    def "should decrease time until next population grow for each fish in swarm"() {
        given:
        def swarm = new LanternFishSwarm([3, 4, 3, 1, 2])

        when:
        swarm.nextDay()

        then:
        swarm.day == 1
        swarm.timers == [1, 1, 2, 1, 0, 0, 0, 0, 0]
    }

    def "should reset to 6 if less than zero and add new fish timer of 8 to swarm"() {
        given:
        def swarm = new LanternFishSwarm([3, 4, 3, 1, 2])

        when:
        swarm.nextDay()
        swarm.nextDay()

        then:
        swarm.day == 2
        swarm.timers == [1, 2, 1, 0, 0, 0, 1, 0, 1]
    }

    def "should populate and reset each day for a given count of days"() {
        given:
        def swarm = new LanternFishSwarm([3, 4, 3, 1, 2])

        when:
        swarm.populate(days)

        then:
        swarm.day == days
        swarm.timers == expected
        swarm.total() == total

        where:
        days || total | expected
        3    || 7     | [2, 1, 0, 0, 0, 1, 1, 1, 1]
        10   || 12    | [3, 2, 2, 1, 0, 1, 1, 1, 1]
        18   || 26    | [3, 5, 3, 2, 2, 1, 5, 1, 4]
    }

    def "should calculate for a lot of days"() {
        given:
        def swarm = new LanternFishSwarm([3, 4, 3, 1, 2])

        when:
        swarm.populate(256)

        then:
        swarm.total() == 26984457539

    }
}
