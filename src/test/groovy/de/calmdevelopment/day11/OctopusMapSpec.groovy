package de.calmdevelopment.day11

import spock.lang.Specification

class OctopusMapSpec extends Specification {
    def "should initialize octopus energy levels"() {
        given:
        def octopusMap = new OctopusMap([
                "5483143223",
                "2745854711",
                "5264556173",
                "6141336146",
                "6357385478",
                "4167524645",
                "2176841721",
                "6882881134",
                "4846848554",
                "5283751526",
        ])

        expect:
        octopusMap.rowSize() == 10
        octopusMap.columnSize() == 10
        octopusMap.flashed() == 0
    }
    def "should mark flashed octopus cells after a step"() {
        given:
        def octopusMap = new OctopusMap([
                "11111",
                "19991",
                "19191",
                "19991",
                "11111"
        ])

        when:
        octopusMap.step()

        then:
        octopusMap.flashed() == 9
    }

    def "should not have flashed octopus cells after 2 steps"() {
        given:
        def octopusMap = new OctopusMap([
                "11111",
                "19991",
                "19191",
                "19991",
                "11111"
        ])

        when:
        octopusMap.step()
        octopusMap.step()

        then:
        octopusMap.flashed() == 0

        octopusMap.rows()[0]*.value == [4,5,6,5,4]
        octopusMap.rows()[1]*.value == [5,1,1,1,5]
        octopusMap.rows()[2]*.value == [6,1,1,1,6]
        octopusMap.rows()[3]*.value == [5,1,1,1,5]
        octopusMap.rows()[4]*.value == [4,5,6,5,4]
    }

    def "should count flashes of all steps"() {
        given:
        def octopusMap = new OctopusMap([
                "5483143223",
                "2745854711",
                "5264556173",
                "6141336146",
                "6357385478",
                "4167524645",
                "2176841721",
                "6882881134",
                "4846848554",
                "5283751526",
        ])

        expect:
        octopusMap.countFlashes(steps) == flashes

        where:
        steps | flashes
        10    | 204
        100   | 1656
    }


    def "should calculate steps until first synchronous"() {
        given:
        def octopusMap = new OctopusMap([
                "5483143223",
                "2745854711",
                "5264556173",
                "6141336146",
                "6357385478",
                "4167524645",
                "2176841721",
                "6882881134",
                "4846848554",
                "5283751526",
        ])

        expect:
        octopusMap.firstSynchronizedStep() == 195

    }

}
