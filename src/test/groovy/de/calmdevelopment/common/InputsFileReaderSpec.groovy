package de.calmdevelopment.common

import de.calmdevelopment.day2.CommandValuePair
import de.calmdevelopment.day3.BitString
import de.calmdevelopment.day4.BingoContext
import de.calmdevelopment.day5.Point
import de.calmdevelopment.day9.HeightMap
import spock.lang.Specification

import static de.calmdevelopment.day2.Command.FORWARD

class InputsFileReaderSpec extends Specification {

    def "should read integer inputs"() {
        given:
        def reader = new InputsFileReader("day1-input")

        expect:
        reader.read().size() == 2000
    }

    def "should read direction value pairs"() {
        given:
        def reader = new InputsFileReader("day2-input")
        def commands = reader.readCommands()
        expect:
        commands.size() == 1000
        commands.first() == new CommandValuePair(command: FORWARD, value: 7)
    }

    def "should read singals"() {
        given:
        def reader = new InputsFileReader("day3-input")
        def data = reader.readDiagnosticData()

        expect:

        data.size() == 1000
        data.first() == new BitString("111110110111")
    }

    def "should read bingo subsystem defintion"() {
        given:
        def reader = new InputsFileReader("day4-input")
        BingoContext bingoContext = reader.readBingoDefinition()

        expect:
        bingoContext.draws.size() == 100
        bingoContext.boards.size() == 100
    }

    def "should read start and endpoints of vent fields"() {
        given:
        def reader = new InputsFileReader("day5-input")
        List<VentField> fields = reader.readVentCoordinates()

        expect:
        fields.size() == 500
        fields[0].start == new Point(941,230)
        fields[0].end == new Point(322,849)
    }

    def "should read fish swarm timers"() {
        given:
        def reader = new InputsFileReader("day6-input")
        def timers = reader.readListOfIntegers()

        expect:
        timers.size() == 300
    }
    def "should read horizontal positions"() {
        given:
        def reader = new InputsFileReader("day7-input")
        def timers = reader.readListOfIntegers()

        expect:
        timers.size() == 1000
    }

    def "should create HeightMap from input"() {
        given:
        def reader = new InputsFileReader("day9-input")

        when:
        HeightMap heightMap = reader.readHeightMap()

        then:
        heightMap.columnSize() == 100
        heightMap.rowSize() == 100
    }
}
