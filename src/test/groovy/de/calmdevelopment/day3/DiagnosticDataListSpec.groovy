package de.calmdevelopment.day3

import spock.lang.Specification

class DiagnosticDataListSpec extends Specification {

    def "should calculate most common bit all one"() {
        given:
        def dataList = [
                new BitString("10110"),
                new BitString("10111"),
        ] as DiagnosticDataList

        expect:
        dataList.mostCommonAt(4)

    }
    def "should calculate least common bit default to false"() {
        given:
        def dataList = [
                new BitString("01111"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        !dataList.leastCommonAt(3)
    }

    def "should calculate most common bit"() {
        given:
        def dataList = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        dataList.mostCommonAt(index) == expected

        where:
        index || expected
        0     || true
        1     || false
        2     || true
        3     || true
        4     || false
    }

    def "should calculate least common bit"() {
        given:
        def dataList = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        dataList.leastCommonAt(index) == expected

        where:
        index || expected
        0     || false
        1     || true
        2     || false
        3     || false
        4     || true
    }

    def "should calculate gamma rate by most common bits"() {
        given:
        def dataList = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        dataList.gammaRate() == 22

    }

    def "should calculate epsilon rate by most common bits"() {
        given:
        def dataList = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        dataList.epsilonRate() == 9

    }

    def "should calculate power consumption"() {
        given:
        def dataList = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        dataList.powerConsumption() == 198

    }

    def "should return sublist of most common criteria"() {
        given:
        def dataList = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList


        def expected = [
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
        ] as DiagnosticDataList
        expect:
        dataList.getMostCommonSublist(0) == expected

    }

    def "should return sublist of least common criteria"() {
        given:
        def list = [
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
        ] as DiagnosticDataList

        def expected = [
                new BitString("11110"),
                new BitString("11100"),
                new BitString("11001")
        ] as DiagnosticDataList

        expect:
        list.getLeastCommonSublist(1) == expected

    }

    def "should calculate oxygen generator rating"() {
        given:
        def list = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        list.oxygenGeneratorRating() == 23

    }

    def "should calculate co2 scrubber rating"() {
        given:
        def list = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        list.co2ScrubberRating() == 10

    }
    def "should calculate life support rating"() {
        given:
        def list = [
                new BitString("00100"),
                new BitString("11110"),
                new BitString("10110"),
                new BitString("10111"),
                new BitString("10101"),
                new BitString("01111"),
                new BitString("00111"),
                new BitString("11100"),
                new BitString("10000"),
                new BitString("11001"),
                new BitString("00010"),
                new BitString("01010"),
        ] as DiagnosticDataList

        expect:
        list.lifeSupportRating() == 230

    }
}
