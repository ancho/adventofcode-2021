package de.calmdevelopment.day8

import spock.lang.Specification

class SegmentDisplaySpec extends Specification {
    def "should split unique signal patterns and four digit outputs"() {
        given:
        def display = new SegmentDisplay()

        when:
        display.addInputline("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")

        then:
        display.inputs.size() == 1
        display.inputs[0].signalPatterns == ["be", "cfbegad", "cbdgef", "fgaecd", "cgeb", "fdcge", "agebfd", "fecdb", "fabcd", "edb"]
        display.inputs[0].outputValue == ["fdgacbe", "cefdb", "cefbgd", "gcbe"]
    }

    def "should find similar patterns in outputValue"() {
        given:
        def display = new SegmentDisplay()
        display.addInputline("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")

        expect:
        display.countMatchingInOutputValue([1, 4, 7, 8]) == 2

    }

    def "should find similar patterns for multiple inputs combined"() {
        given:
        def display = new SegmentDisplay()

        display.addInputline("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")
        display.addInputline("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc")
        display.addInputline("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg")
        display.addInputline("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb")
        display.addInputline("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea")
        display.addInputline("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb")
        display.addInputline("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe")
        display.addInputline("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef")
        display.addInputline("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb")
        display.addInputline("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")

        expect:
        display.countMatchingInOutputValue([1, 4, 7, 8]) == 26

    }

    def "should calculate output values for each input entry"() {
        given:
        def display = new SegmentDisplay()

        display.addInputline("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe")
        display.addInputline("edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc")
        display.addInputline("fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg")
        display.addInputline("fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb")
        display.addInputline("aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea")
        display.addInputline("fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb")
        display.addInputline("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe")
        display.addInputline("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef")
        display.addInputline("egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb")
        display.addInputline("gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce")

        expect:
        display.inputs[0].getOutputValues() == 8394
        display.inputs[1].getOutputValues() == 9781
        display.inputs[2].getOutputValues() == 1197
        display.inputs[3].getOutputValues() == 9361
        display.inputs[4].getOutputValues() == 4873
        display.inputs[5].getOutputValues() == 8418
        display.inputs[6].getOutputValues() == 4548
        display.inputs[7].getOutputValues() == 1625
        display.inputs[8].getOutputValues() == 8717
        display.inputs[9].getOutputValues() == 4315


    }

    def "should create segment digits map from input"() {
        given:
        def display = new SegmentDisplay()
        display.addInputline("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")
        def entry = display.inputs[0]
        when:
        def segmentsMap = entry.patterns.sort()

        then:
        segmentsMap.sort() == [
                8: "abcdefg",
                5: "bcdef",
                2: "acdfg",
                3: "abcdf",
                7: "abd",
                9: "abcdef",
                6: "bcdefg",
                4: "abef",
                0: "abcdeg",
                1: "ab"
        ].sort()
    }

    def "should calculate outputs from inputs segments"() {
        given:
        def display = new SegmentDisplay()
        display.addInputline("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")
        def entry = display.inputs[0]
        expect:
        entry.getOutputValues() == 5353
    }

    def "should contain pattern"() {
        given:
        def display = new SegmentDisplay()
        display.addInputline("bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef")
        def entry = display.inputs[0]

        expect:
        entry.getOutputValues() == 1625
    }

    def "should contain pattern e"() {
        given:
        def display = new SegmentDisplay()
        display.addInputline("dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe")
        def entry = display.inputs[0]

        expect:
        entry.getOutputValues() == 4548
    }
}
