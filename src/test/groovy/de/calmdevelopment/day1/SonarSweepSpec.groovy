package de.calmdevelopment.day1

import spock.lang.Specification

class SonarSweepSpec extends Specification {
    Reader reader
    List<Integer> depths

    void setup() {
        reader = Mock(Reader)
        depths = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
    }

    def "should return sweep report as list of see floor depth from file reader"() {
        given:
        reader.read() >> depths
        def sweep = new SonarSweep(reader)

        expect:
        sweep.report() == depths
    }

    def "should count zero increases if empty "() {
        given:
        reader.read() >> []
        def sweep = new SonarSweep(reader)
        def report = sweep.report()

        expect:
        sweep.increases(report) == 0
    }

    def "should count zero increases only on element"() {
        given:
        reader.read() >> [199]
        def sweep = new SonarSweep(reader)
        def report = sweep.report()

        expect:
        sweep.increases(report) == 0
    }

    def "should count one increases if next element is deeper"() {
        given:
        reader.read() >> [199, 200]
        def sweep = new SonarSweep(reader)
        def report = sweep.report()

        expect:
        sweep.increases(report) == 1
    }

    def "should count zero increases if next element is shallow"() {
        given:
        reader.read() >> [199, 198]
        def sweep = new SonarSweep(reader)
        def report = sweep.report()

        expect:
        sweep.increases(report) == 0
    }

    def "should count 7 increases for given report"() {
        given:
        reader.read() >> depths
        def sweep = new SonarSweep(reader)
        def report = sweep.report()

        expect:
        sweep.increases(report) == 7
    }

    def "should count 7 increases with uneven count of given inputs"() {
        given:
        depths << 261
        reader.read() >> depths
        def sweep = new SonarSweep(reader)
        def report = sweep.report()

        expect:
        sweep.increases(report) == 7
    }

    def "should count with sliding window"() {
        given:
        reader.read() >> depths
        def sweep = new SonarSweep(reader)
        def report = sweep.slidingWindowReport()

        expect:
        sweep.increases(report) == 5
    }
}
