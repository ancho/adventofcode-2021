package de.calmdevelopment.day1

import spock.lang.Specification

class SlidingWindowReportSpec extends Specification {
    def "should sum values by window size"() {
        given:
        def window = new SlidingWindow()
        def input = [199, 200, 208]

        when:
        def report = window.create(input)

        then:
        report == [607]
    }

    def "should slide and collect sums by size"() {
        given:
        def window = new SlidingWindow()
        def input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

        when:
        def report = window.create(input)

        then:
        report == [607, 618, 618, 617, 647, 716, 769, 792]
    }


}
