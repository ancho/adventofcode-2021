package de.calmdevelopment.day1

import de.calmdevelopment.day2.CommandValuePair
import de.calmdevelopment.day2.Submarine
import spock.lang.Specification

import static de.calmdevelopment.day2.Command.FORWARD
import static de.calmdevelopment.day2.Command.DOWN
import static de.calmdevelopment.day2.Command.UP

class SubmarineSpec extends Specification {

    def "should start with a depth of zero and a horizontal position of zero"() {
        given:
        def sub = new Submarine()

        expect:
        sub.depth == 0
        sub.horizontalPosition == 0
        sub.aim == 0
    }

    def "should move forward, increase horizontal position and calculate depth with aim"() {
        given:
        def sub = new Submarine(aim: 2)
        def forward = new CommandValuePair(command: FORWARD, value: 5)

        when:
        sub.navigate(forward)

        then:
        sub.horizontalPosition == 5
        sub.depth == 10
        sub.aim == 2
    }

    def "should move down"() {
        given:
        def sub = new Submarine()
        def down = new CommandValuePair(command: DOWN, value: 5)

        when:
        sub.navigate(down)

        then:
        sub.horizontalPosition == 0
        sub.depth == 0
        sub.aim == 5
    }

    def "should move up"() {
        given:
        def sub = new Submarine(aim: 5)
        def up = new CommandValuePair(command: UP, value: 3)

        when:
        sub.navigate(up)

        then:
        sub.horizontalPosition == 0
        sub.depth == 0
        sub.aim == 2
    }

    def "should navigate a submarine"() {
        given:
        def sub = new Submarine()
        def cmds = [
                new CommandValuePair(command: FORWARD, value: 5),
                new CommandValuePair(command: DOWN, value: 5),
                new CommandValuePair(command: FORWARD, value: 8),
                new CommandValuePair(command: UP, value: 3),
                new CommandValuePair(command: DOWN, value: 8),
                new CommandValuePair(command: FORWARD, value: 2)
        ]

        when:
        sub.navigate(cmds)

        then:
        sub.depth == 60
        sub.horizontalPosition == 15
        sub.aim == 10

    }

    def "should not go higher then 0"() {
        given:
        def sub = new Submarine()
        def cmds = [
                new CommandValuePair(command: DOWN, value: 5),
                new CommandValuePair(command: UP, value: 8)
        ]

        when:
        sub.navigate(cmds)

        then:
        sub.depth == 0
    }
}
