package de.calmdevelopment.day4

import spock.lang.Specification

class BingoBoardSpec extends Specification {
    def "should should be unmarked"() {
        given:
        def board = new BingoBoard()
        board.addRow("22 13 17 11  0")
        board.addRow(" 8  2 23  4 24")
        board.addRow("21  9 14 16  7")
        board.addRow(" 6 10  3 18  5")
        board.addRow(" 1 12 20 15 19")

        expect:
        board.rows().each { row -> row.each { col -> !col.marked } }
    }

    def "should mark number if drawn"() {
        given:
        def board = new BingoBoard()
        board.addRow("22 13 17 11  0")
        board.addRow(" 8  2 23  4 24")
        board.addRow("21  9 14 16  7")
        board.addRow(" 6 10  3 18  5")
        board.addRow(" 1 12 20 15 19")

        when:
        board.draw(14)

        then:
        board.rows()[2][2].value == 14
        board.rows()[2][2].marked
        board.lastDraw == 14
    }

    def "should win if all values in row are marked"() {
        given:
        def board = new BingoBoard()
        board.addRow("14 21 17 24  4")
        board.addRow("10 16 15  9 19")
        board.addRow("18  8 23 26 20")
        board.addRow("22 11 13  6  5")
        board.addRow(" 2  0 12  3  7")

        when:
        board.draw(14)
        board.draw(21)
        board.draw(17)
        board.draw(24)
        board.draw(4)

        then:
        board.win()
        board.wonNumbers() == [14, 21, 17, 24, 4]
        board.lastDraw == 4
        board.bingoRow == 0
    }
    def "should calculate score from unmarked values and multiplied by last drawn number"() {
        given:
        def board = new BingoBoard()
        board.addRow("14 21 17 24  4")
        board.addRow("10 16 15  9 19")
        board.addRow("18  8 23 26 20")
        board.addRow("22 11 13  6  5")
        board.addRow(" 2  0 12  3  7")

        when:
        def draws = [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24]
        draws.each {board.draw(it)}

        then:
        board.win()
        board.wonNumbers() == [14, 21, 17, 24, 4]
        board.lastDraw == 24
        board.score() == 4512
    }

    def "should win if all values in col are marked"() {
        given:
        def board = new BingoBoard()
        board.addRow("22 13 17 11  0")
        board.addRow(" 8  2 23  4 24")
        board.addRow("21  9 14 16  7")
        board.addRow(" 6 10  3 18  5")
        board.addRow(" 1 12 20 15 19")

        when:
        board.draw(11)
        board.draw(4)
        board.draw(16)
        board.draw(18)
        board.draw(15)

        then:
        board.win()
        board.wonNumbers() == [11, 4, 16, 18, 15]
    }


}
