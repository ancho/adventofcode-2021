package de.calmdevelopment.day4

import spock.lang.Specification

class BingoGameSpec extends Specification {

    def "should resolve first board that wins"() {
        given:
        def draws = [7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24]
        def board1 = new BingoBoard()
        board1.addRow("14 21 17 24  4")
        board1.addRow("10 16 15  9 19")
        board1.addRow("18  8 23 26 20")
        board1.addRow("22 11 13  6  5")
        board1.addRow(" 2  0 12  3  7")

        def board2 = new BingoBoard()
        board2.addRow("3 15  0  2 22")
        board2.addRow("9 18 13 17  5")
        board2.addRow("19  8  7 25 23")
        board2.addRow("20 11 10 24  4")
        board2.addRow("14 21 16 12  6")

        def board3 = new BingoBoard()
        board3.addRow("22 13 17 11  0")
        board3.addRow(" 8  2 23  4 24")
        board3.addRow("21  9 14 16  7")
        board3.addRow(" 6 10  3 18  5")
        board3.addRow(" 1 12 20 15 19")

        def context = new BingoContext(draws: draws)
        context.addBoard(board1)
        context.addBoard(board2)
        context.addBoard(board3)

        def game = new BingoGame(context)

        when:
        def winBoard = game.solve()

        then:
        winBoard.lastDraw == 24
        winBoard.wonNumbers() == [14, 21, 17, 24, 4]
    }

    def "should resolve last board wins"() {
        given:
        def draws = [7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1]
        def board3 = new BingoBoard()
        board3.addRow("14 21 17 24  4")
        board3.addRow("10 16 15  9 19")
        board3.addRow("18  8 23 26 20")
        board3.addRow("22 11 13  6  5")
        board3.addRow(" 2  0 12  3  7")

        def board2 = new BingoBoard()
        board2.addRow("3 15  0  2 22")
        board2.addRow("9 18 13 17  5")
        board2.addRow("19  8  7 25 23")
        board2.addRow("20 11 10 24  4")
        board2.addRow("14 21 16 12  6")

        def board1 = new BingoBoard()
        board1.addRow("22 13 17 11  0")
        board1.addRow(" 8  2 23  4 24")
        board1.addRow("21  9 14 16  7")
        board1.addRow(" 6 10  3 18  5")
        board1.addRow(" 1 12 20 15 19")

        def context = new BingoContext(draws: draws)
        context.addBoard(board1)
        context.addBoard(board2)
        context.addBoard(board3)

        def game = new BingoGame(context)

        when:
        def winBoard = game.solveLast()

        then:
        winBoard.lastDraw == 13
        winBoard.score() == 1924
    }
}

