package de.calmdevelopment.day5

import spock.lang.Specification

class HydrothermalVentMapSpec extends Specification {
    def "should grow rows by given coordinates"() {
        given:
        def map = new HydrothermalVentMap()

        when:
        map.addVent(new Point(0,9), new Point(5,9))

        then:
        map.rows().size() == 10
        map.rows()[0].size() == 6
    }

    def "should increment vents count for line"() {
        given:
        def map = new HydrothermalVentMap()

        when:
        map.addVent(new Point(0,9), new Point(5,9))

        then:
        map.rows()[9].collect {it.value} == [1,1,1,1,1,1]
    }

    def "should increment multiple added vents"() {
        given:
        def map = new HydrothermalVentMap()

        when:
        map.addVent(new Point(0,9), new Point(5,9))
        map.addVent(new Point(8,0), new Point(0,8))
        map.addVent(new Point(9,4), new Point(3,4))
        map.addVent(new Point(2,2), new Point(2,1))
        map.addVent(new Point(7,0), new Point(7,4))
        map.addVent(new Point(6,4), new Point(2,0))
        map.addVent(new Point(0,9), new Point(2,9))
        map.addVent(new Point(3,4), new Point(1,4))
        map.addVent(new Point(0,0), new Point(8,8))
        map.addVent(new Point(5,5), new Point(8,2))

        then:
        map.rows()[0].collect {it.value} == [1,0,1,0,0,0,0,1,1,0]
        map.rows()[1].collect {it.value} == [0,1,1,1,0,0,0,2,0,0]
        map.rows()[2].collect {it.value} == [0,0,2,0,1,0,1,1,1,0]
        map.rows()[3].collect {it.value} == [0,0,0,1,0,2,0,2,0,0]
        map.rows()[4].collect {it.value} == [0,1,1,2,3,1,3,2,1,1]
        map.rows()[5].collect {it.value} == [0,0,0,1,0,2,0,0,0,0]
        map.rows()[6].collect {it.value} == [0,0,1,0,0,0,1,0,0,0]
        map.rows()[7].collect {it.value} == [0,1,0,0,0,0,0,1,0,0]
        map.rows()[8].collect {it.value} == [1,0,0,0,0,0,0,0,1,0]
        map.rows()[9].collect {it.value} == [2,2,2,1,1,1,0,0,0,0]

    }

    def "should count min two overlaps"() {
        given:
        def map = new HydrothermalVentMap()

        when:
        map.addVent(new Point(0,9), new Point(5,9))
        map.addVent(new Point(8,0), new Point(0,8))
        map.addVent(new Point(9,4), new Point(3,4))
        map.addVent(new Point(2,2), new Point(2,1))
        map.addVent(new Point(7,0), new Point(7,4))
        map.addVent(new Point(6,4), new Point(2,0))
        map.addVent(new Point(0,9), new Point(2,9))
        map.addVent(new Point(3,4), new Point(1,4))
        map.addVent(new Point(0,0), new Point(8,8))
        map.addVent(new Point(5,5), new Point(8,2))

        then:
        map.numberMin2Overlaps() == 12
    }

    def "should grow columns by given coordinates"() {
        given:
        def map = new HydrothermalVentMap()
        map.addVent(new Point(0,9), new Point(5,9))

        when:
        map.addVent(new Point(8,0), new Point(0,8))

        then:
        map.rows().size() == 10
        map.rows()[0].size() == 9

    }

}
