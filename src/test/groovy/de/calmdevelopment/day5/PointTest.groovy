package de.calmdevelopment.day5

import spock.lang.Specification

class PointTest extends Specification {

    def "should return empty list if start equals end"() {
        given:
        def start = new Point(9,7)
        def end = new Point(9,7)

        expect:
        start.coverPoints(end) == []

    }

    def "should calculate cover points from given coordinates"() {
        expect:
        start.coverPoints(end) == expected

        where:
        start           | end               || expected
        new Point(1,1)  | new Point(1,3)    || [new Point(1,1), new Point(1,2), new Point(1,3)]
        new Point(7,0)  | new Point(7,4)    || [new Point(7,0), new Point(7,1), new Point(7,2), new Point(7,3), new Point(7,4)]
        new Point(2,2)  | new Point(2,1)    || [new Point(2,1), new Point(2,2)]
    }

    def "should calculate horizontal cover points from given coordinates"() {
        expect:
        start.coverPoints(end) == expected

        where:
        start           | end               || expected
        new Point(9,7)  | new Point(7,7)    || [new Point(7,7), new Point(8,7), new Point(9,7)]
        new Point(7,7)  | new Point(9,7)    || [new Point(7,7), new Point(8,7), new Point(9,7)]
        new Point(0,9)  | new Point(5,9)    || [new Point(0,9), new Point(1,9), new Point(2,9), new Point(3,9), new Point(4,9), new Point(5,9)]
    }

    def "should cover diagonal points"() {
        expect:
        start.coverPoints(end) == expected

        where:
        start           | end               || expected
        new Point(1,1)  | new Point(3,3)    || [new Point(1,1), new Point(2,2), new Point(3,3)]
        new Point(3,3)  | new Point(0,0)    || [new Point(0,0), new Point(1,1), new Point(2,2), new Point(3,3)]
        new Point(9,7)  | new Point(7,9)    || [new Point(9,7), new Point(8,8), new Point(7,9)]
        new Point(2,0)  | new Point(6,4)    || [new Point(2,0), new Point(3,1), new Point(4,2), new Point(5,3), new Point(6,4)]
        new Point(6,4)  | new Point(2,0)    || [new Point(2,0), new Point(3,1), new Point(4,2), new Point(5,3), new Point(6,4)]
        new Point(8,0)  | new Point(0,8)    || [new Point(8,0), new Point(7,1), new Point(6,2), new Point(5,3), new Point(4,4), new Point(3,5), new Point(2,6), new Point(1,7), new Point(0,8)]
        new Point(0,8)  | new Point(8,0)    || [new Point(8,0), new Point(7,1), new Point(6,2), new Point(5,3), new Point(4,4), new Point(3,5), new Point(2,6), new Point(1,7), new Point(0,8)]
    }

}
