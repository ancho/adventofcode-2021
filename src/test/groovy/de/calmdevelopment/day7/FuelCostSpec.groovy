package de.calmdevelopment.day7

import spock.lang.Specification

class FuelCostSpec extends Specification {

    def "should calculate fuel cost at given horizontal index"() {
        given:
        def cost = new FuelCost([16,1,2,0,4,2,7,1,2,14])

        expect:
        cost.calculate(index) == expected

        where:
        index   || expected
        5       || 168
        2       || 206
        3       || 183
        10      || 311
    }

    def "should find horizontal with smallest cost of fuel"() {
        given:
        def cost = new FuelCost([16,1,2,0,4,2,7,1,2,14])

        expect:
        cost.leastFuelCost() == 168
    }
}
