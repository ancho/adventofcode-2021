package de.calmdevelopment.day9

import de.calmdevelopment.day4.Cell
import de.calmdevelopment.day5.Point
import spock.lang.Specification
import spock.lang.Unroll

class HeightMapSpec extends Specification {
    @Unroll
    def "should return adjacent locations by index #row,#col"() {
        given:
        def input = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """
        def heightMap = createHeightMap(input)

        expect:
        heightMap.adjacentListAt(new Point(col, row)).sort() == expected.sort()

        where:
        row | col || expected
        0   | 0   || [new Cell(value: 1, location: new Point(1, 0)), new Cell(value: 3, location: new Point(0, 1))]
        0   | 1   || [new Cell(value: 2, location: new Point(0, 0)), new Cell(value: 9, location: new Point(2, 0)), new Cell(value: 9, location: new Point(1, 1))]
        2   | 4   || [new Cell(value: 8, location: new Point(4, 1)), new Cell(value: 8, location: new Point(4, 3)), new Cell(value: 8, location: new Point(5, 2)), new Cell(value: 6, location: new Point(3, 2))]
        0   | 9   || [new Cell(value: 1, location: new Point(8, 0)), new Cell(value: 1, location: new Point(9, 1))]
    }

    @Unroll
    def "should mark all cells which are lower than adjacent cells"() {
        given:
        def input = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """
        def heightMap = createHeightMap(input)

        when:
        heightMap.markLowest()

        then:
        heightMap.getAt(row, col).marked

        where:
        row | col
        0   | 1
        0   | 9
        2   | 2
        2   | 2
        4   | 6

    }

    def "should calculate low point risk level"() {
        given:
        def input = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """
        def heightMap = createHeightMap(input)

        expect:
        heightMap.lowPointRiskLevel() == 15

    }

    def "should get basin for lower cell"() {
        given:
        def input = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """
        def heightMap = createHeightMap(input)
        def cell = heightMap.getLowest()[0]

        expect:
        heightMap.basin(cell).size() == 3
    }

    def "should calculate tree largest basins"() {
        given:
        def input = """
        2199943210
        3987894921
        9856789892
        8767896789
        9899965678
        """
        def heightMap = createHeightMap(input)

        expect:
        heightMap.largestBasins() == 1134
    }


    private HeightMap createHeightMap(String input) {
        def heightMap = new HeightMap()
        input.readLines().findAll({ !it.isEmpty() }).each {
            heightMap.addRow(it.trim().collect { new Cell(value: it as int) })
        }
        heightMap
    }
}
